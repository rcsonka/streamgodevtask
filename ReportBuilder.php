<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\Session;

class ReportBuilder extends Model
{

  private $report_type;
  private $db_query;

  public function __construct($report_type)
  {
    $this->report_type = $report_type;
  }


  public function retrieve()
  {
      switch($this->report_type){
        case "Sessions":
          $sessions = new Session;
          $this->db_query = $sessions;
        break;
        case "Clients":
          $this->db_query = DB::table('clients');
        break;
      }
      return $this;
  }

  public function get()
  {
    return $this->retrieve()->db_query->get();
  }

  public function export()
  {
      $data = $this->retrieve()->db_query->get()->toArray();
      $filename = 'filename.csv';

      $headers = [
        //example headers
       ];

      return $return = ['data'=>$data,'headers'=>$headers];
  }
}
